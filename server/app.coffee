import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'
import queryString from 'query-string'
import randomize from 'randomatic'
import braintree from 'braintree'
wepay = require('wepay').WEPAY
import Future from 'fibers/future'



TEST_MODE = no
PAYMENT_SERVICE = 'Braintree'



Sessions = new Mongo.Collection 'sessions'
Testimonials = new Mongo.Collection 'testimonials'



Meteor.startup ->
  console.log "----------------------------------------------------------------------------"
  console.log "|                        Server started successfully                       |"
  console.log "----------------------------------------------------------------------------"



  if not Meteor.users.find({ username: 'hexa.admin' }).fetch()[0]
    Accounts.createUser
      username: 'hexa.admin'
      email: 'hello@hexa.systems'
      password: 'Hellohexa1'
      profile:
        admin: yes



  process.env.MAIL_URL = 'smtps://vlad.dewitt:cvsYW5MhrsyMaL5@smtp.sendgrid.net:465'



  if PAYMENT_SERVICE is 'Braintree'
    if TEST_MODE
      gateway = braintree.connect
        environment: braintree.Environment.Sandbox
        merchantId: '4xxxcgk78h6pkngr'
        publicKey: 'k3f3mgmwyt3gxfvh'
        privateKey: 'b84b0ae369532bb3598334f883e8df86'
    else
      gateway = braintree.connect
        environment: braintree.Environment.Production
        merchantId: 'xfch8w3p9dyxmg96'
        publicKey: 'cm6qcj5h7tyjczsy'
        privateKey: '3481a92ab857f0e340f4654b0aca28f9'

  else if PAYMENT_SERVICE is 'WePay'
    if TEST_MODE
      wepay_settings =
        'client_id': '13257'
        'client_secret': '9822b953f0'
        'access_token': 'STAGE_6f0791088e826ce42f8759d31b18324f1e228874e01dc7bb34040d4490c09325'
        'account_id': 1605862514
      wp = new wepay wepay_settings
      wp.use_staging()
    else
      wepay_settings =
        'client_id': '161410'
        'client_secret': '8651c1a6d9'
        'access_token': 'PRODUCTION_afb85b70d349d1738a7936efa8ab130db75dbd9cada63bfeab1550b94540b5be'
        'account_id': 1300619023
      wp = new wepay wepay_settings
      wp.use_production()



  Meteor.publish 'sessions', ->
    if Meteor.user()
      if Meteor.user().profile.admin
        Sessions.find()

  Meteor.publish 'testimonials', ->
    Testimonials.find()



  Meteor.methods
    getAdvice: (query) ->
      advice_key = queryString.parse(query).advice

      if TEST_MODE
        if advice_key is 'h1kf05aie8301g73'
          {
            code: "S"
            message: "The Advice successfully found."
            advice: {
              title: "Title"
              subtitle: "Subtitle"
              text: '''
                Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.
                Lorem ipsum dolor sit amet.

                Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.
                Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.

                Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.
              '''
            }
          }
        else
          {
            code: "F"
            message: "Adices not found."
          }
      else
        advice_search_result = Sessions.find({ 'advice_key': advice_key }).fetch()[0]

        if advice_search_result
          {
            code: "S"
            message: "The Advice successfully found."
            advice: advice_search_result.answer
          }
        else
          {
            code: "F"
            message: "Adices not found."
          }




    submitEmail: (session_id, email) ->
      searchSession = Sessions.find({ id: session_id }).fetch()
      confirmation_code = randomize '000'

      if searchSession[0] is undefined
        Sessions.insert
          id: session_id
          email: email
          verification_code: confirmation_code
          payment: {}
          paid: no
          answered: no
          sended: no
          createdAt: new Date()
          answer: {}
          advice_key: ''
          removed: no
      else
        Sessions.update({ id: session_id }, { $set: { email: email, verification_code: confirmation_code } })

      Meteor.call 'sendVerificationEmail', email, confirmation_code



    resendVerificationCode: (session_id) ->
      confirmation_code = randomize '000'

      Sessions.update({ id: session_id }, { $set: { verification_code: confirmation_code } })

      Meteor.call 'sendVerificationEmail', Sessions.find({ id: session_id }).fetch()[0].email, confirmation_code



    sendVerificationEmail: (email, confirmation_code) ->
      if not TEST_MODE
        html = "<div style='width: 360px; padding: 48px; background-color: #444D6A; text-align: left; border-radius: 24px; color: white;'><img style='width: 180px; margin-bottom: 1.5em;' src='https://tenhundred.com/img/pictures/logo_title.png'/><h3 style=\"font-family: 'Futura', 'Trebuchet MS', 'Arial', 'sans-serif'; letter-spacing: 1px; font-weight: bold;\">This is your email verification code:</h3><h1 style=\"margin: 0.2em 0 0; font-family: 'Futura', 'Trebuchet MS', 'Arial', 'sans-serif'; font-size: 300%; font-weight: bold; letter-spacing: 3px;\">#{ confirmation_code }</h1></div>"

        Email.send
          from: 'Ten Hundred <hello@tenhundred.com>'
          to: email
          subject: "Your TenHundred verification code"
          html: html



    checkVerificationCode: (session_id, code) ->
      if TEST_MODE
        session_code = '444'
      else
        session_code = Sessions.find({ id: session_id }).fetch()[0].verification_code

      if code is session_code
        {
          code: "S"
          message: "Your email was verified successfully. In the next step, we will ask you to enter your credit card details. Please be confident that we won’t be charging your card until your letter is answered."
        }
      else
        {
          code: "F"
          message: "You entered the wrong code. Please check your email again."
        }



    submitCreditCard: (session_id, credit_card) ->
      future = new Future()

      if PAYMENT_SERVICE is 'Braintree'
        gateway.customer.create
          id: session_id
          email: Sessions.find({ id: session_id }).fetch()[0].email
          creditCard:
            number: credit_card.card_number
            expirationDate: credit_card.exp_date
            cvv: credit_card.cvv
            options:
              verifyCard: yes
              verificationAmount: "50.00"
        , Meteor.bindEnvironment (err, res) =>
          if err
            console.log '[ CUSTOMER_CREATE - Server Error ]:'
            console.log err
            future.return
              code: "F"
              message: err
          else
            if res.success
              Sessions.update({ id: session_id }, { $set: { payment_method: res.transaction, payment_method_verified: res.success } })
              console.log '[ CUSTOMER_CREATE - Success ]'
              future.return
                code: "S"
                message: res
            else
              console.log '[ CUSTOMER_CREATE - Error ]:'
              console.log res
              future.return
                code: "F"
                message: res

      else if PAYMENT_SERVICE is 'WePay'
        cc_data =
          'client_id': wepay_settings.client_id
          'cc_number': credit_card.card_number
          'expiration_month': parseInt credit_card.exp_date.split('/')[0], 10
          'expiration_year': 2000 + parseInt credit_card.exp_date.split('/')[1], 10
          'user_name': credit_card.name
          'email': Sessions.find({ id: session_id }).fetch()[0].email
          'address':
            'postal_code': credit_card.zipcode
          'cvv': credit_card.cvv

        wp.call '/credit_card/create', cc_data, Meteor.bindEnvironment (cc_res) ->
          if cc_res.error
            future.return
              code: "F"
              message: cc_res
          else
            checkout_data =
              'account_id': wepay_settings.account_id
              'short_description': "Ten Hundred - Payment for Advice"
              'type': 'service'
              'currency': 'USD'
              'amount': 50
              'payment_method':
                'type': 'credit_card'
                'credit_card':
                  'id': cc_res.credit_card_id
              'fee':
                'fee_payer': 'payee'

            wp.call '/checkout/create', checkout_data, Meteor.bindEnvironment (checkout_res) ->
              if checkout_res.error or checkout_res.state isnt 'authorized'
                future.return
                  code: "F"
                  message: checkout_res
              else
                Sessions.update({ id: session_id }, { $set: { payment: checkout_res, paid: yes } })
                future.return
                  code: "S"
                  message: checkout_res

      future.wait()



    sendQuestion: (session_id, question) ->
      Sessions.update({ id: session_id }, { $set: { question: question } })

      email = Sessions.find({ id: session_id }).fetch()[0].email

      html = "<div style='width: 360px; padding: 48px; background-color: #444D6A; text-align: left; border-radius: 24px; color: white;'><img style='width: 180px; margin-bottom: 1.5em;' src='https://tenhundred.com/img/pictures/logo_title.png'/><h3 style=\"font-family: 'Futura', 'Trebuchet MS', 'Arial', 'sans-serif'; letter-spacing: 1px; font-weight: bold;\">Your question has been received. We will send a letter to your email within the next 48 hours with your ready advice.</h3></div>"

      Email.send
        from: 'Ten Hundred <hello@tenhundred.com>'
        to: email
        subject: "Your question has been received"
        html: html



    removeSession: (id) ->
      if Meteor.userId()
        if Meteor.user().profile.admin
          session = Sessions.find({ id: id }).fetch()[0]
          if not session.paid and not session.answered and not session.sended or session.paid and session.answered and session.sended
            Sessions.update({ id: id }, { $set: { removed: yes } })



    addTestimonial: (data) ->
      if Meteor.userId()
        if Meteor.user().profile.admin
          if data.testimonial.length > 10
            Testimonials.insert({ data..., createdAt: new Date() })



    removeTestimonial: (id) ->
      if Meteor.userId()
        if Meteor.user().profile.admin
          Testimonials.remove id



    saveAnswer: (id, answer) ->
      if Meteor.userId()
        if Meteor.user().profile.admin
          Sessions.update({ id: id }, { $set: { answer: answer, answered: yes } })



    sendAnswer: (id) ->
      if Meteor.userId()
        if Meteor.user().profile.admin
          gateway.transaction.sale
            customerId: id
            amount: '50.00'
          , Meteor.bindEnvironment (err, res) =>
            if err
              console.log '[ PAYMENT - Error ]'
              console.log err
            else
              if res.success
                console.log '[ PAYMENT - Success ]'

                session = Sessions.find({ id: id }).fetch()[0]

                key = randomize 'A0', 16

                Sessions.update({ id: id }, { $set: { advice_key: key, paid: yes, sended: yes } })

                # html = "<h3>Your Advice is ready! Take it right now:</h3><a href='#{ Meteor.absoluteUrl() }?advice=#{ key }'>#{ Meteor.absoluteUrl() }?advice=#{ key }</a>"
                html = "<div style='width: 360px; padding: 48px; background-color: #444D6A; text-align: left; border-radius: 24px; color: white;'><img style='width: 180px; margin-bottom: 1.5em;' src='https://tenhundred.com/img/pictures/logo_title.png'/><h4 style=\"font-family: 'Futura', 'Trebuchet MS', 'Arial', 'sans-serif'; letter-spacing: 1px; font-weight: bold;\">Your Advice is ready! Take it right now:</h4><a style='color: white;' href='#{ Meteor.absoluteUrl() }?advice=#{ key }'>#{ Meteor.absoluteUrl() }?advice=#{ key }</a></div>"

                Email.send
                  from: 'Ten Hundred <hello@tenhundred.com>'
                  to: session.email
                  subject: "Take Your Advice!"
                  html: html

              else
                console.log '[ PAYMENT - Error ]'
                console.log err



    resendAnswer: (id) ->
      session = Sessions.find({ id: id }).fetch()[0]

      key = randomize 'A0', 16

      Sessions.update({ id: id }, { $set: { advice_key: key } })

      # html = "<h3>Your Advice is ready! Take it right now:</h3><a href='#{ Meteor.absoluteUrl() }?advice=#{ key }'>#{ Meteor.absoluteUrl() }?advice=#{ key }</a>"
      html = "<div style='width: 360px; padding: 48px; background-color: #444D6A; text-align: left; border-radius: 24px; color: white;'><img style='width: 180px; margin-bottom: 1.5em;' src='http://52.15.229.127/img/pictures/logo_title.png'/><h4 style=\"font-family: 'Futura', 'Trebuchet MS', 'Arial', 'sans-serif'; letter-spacing: 1px; font-weight: bold;\">Your Advice is ready! Take it right now:</h4><a style='color: white;' href='#{ Meteor.absoluteUrl() }?advice=#{ key }'>#{ Meteor.absoluteUrl() }?advice=#{ key }</a></div>"

      Email.send
        from: 'Ten Hundred <hello@tenhundred.com>'
        to: session.email
        subject: "Take Your Advice!"
        html: html
