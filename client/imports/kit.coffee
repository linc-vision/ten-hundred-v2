import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'

import Cards from './components/cards/kit'
import Admin from './components/admin/kit'
import MobileCards from './components/mobile_cards/kit'



App = class extends Component
  constructor: (props) ->
    super props
    @state = {}



  componentWillReceiveProps: (newProps) =>
    if @props.tracker.userLoaded isnt newProps.tracker.userLoaded
      @props.loadUser()



  mouseMove: (e) =>
    mouseCoordinates =
      x: if e.clientX > 0 then e.clientX else 1
      y: if e.clientY > 0 then e.clientY else 1
    $('.cards_layout').css
      transform: "translate3d(-50%, -50%, 0) rotateX(#{ (mouseCoordinates.y - $(window).height() / 2) / 600 }deg) rotateY(#{ (mouseCoordinates.x - $(window).width() / 2) / 600 }deg)"
    $('.active_card > #layout').css
      transform: "translate3d(#{ (mouseCoordinates.x - $(window).width() / 2) / 20 }px, #{ (mouseCoordinates.y - $(window).height() / 2) / 20 }px, 0)"



  render: =>
    if not @props.app.mobile
      <div id='App' onMouseMove={ if @props.app.parallax then @mouseMove }>
        <div id='layout'>
          <Switch>
            <Route exact path='/' render={ () => <Cards /> }/>
            <Route path='/admin' render={ () => <Admin /> }/>
            <Redirect path='*' to='/'/>
          </Switch>
          {
            if @props.testimonials.length > 0
              <Route exact path='/' render={ () =>
                <div id='testimonials_button' className={ if @props.app.cards.state isnt 1 then 'hidden' }>
                  <div id='layer_big'></div>
                  <div id='layer_small'></div>
                  <div className='big_button' onClick={ @props.changeCardsPosition.bind this, 15 }>
                    <div id='icon'></div>
                    <p>Testimonials</p>
                  </div>
                </div>
              }/>
          }
        </div>
      </div>
    else
      <div id='App'>
        <div id='layout' className='mobile'>
          <MobileCards />
        </div>
      </div>



export default outfit App
