import mobile from 'is-mobile'
import randomize from 'randomatic'

import cardsPositions from './reservoir/cards_positions'



origin =
  mobile: mobile()
  user: Meteor.userId()
  PAYMENT_SERVICE: 'Braintree'
  session:
    id: null
  parallax: yes
  cards: cardsPositions[1]
  adviceCard: no
  advice: {}



export default (state = origin, action) ->
  switch action.type

    when 'LOAD_USER'
      state = {
        state...
        user: Meteor.user()
      }

    when 'CREATE_SESSION'
      state = {
        state...
        session:
          id: randomize 'A0', 16
      }

    when 'SHOW_ADVICE_CARD'
      state = {
        state...
        adviceCard: yes
        advice: action.payload
      }

    when 'CHANGE_CARDS_POSITION'
      switch action.payload
        when 1
          state = {
            state...
            cards: cardsPositions[1]
          }
        when 2
          analytics.track 'Clicked "Get Advice" button'

          state = {
            state...
            cards: cardsPositions[2]
          }
        when 3
          analytics.track 'User submitted email address'

          state = {
            state...
            cards: cardsPositions[3]
          }
        when 4
          analytics.track 'User verified email address'

          state = {
            state...
            cards: cardsPositions[4]
          }
        when 5
          analytics.track 'User made payment'

          state = {
            state...
            cards: cardsPositions[5]
          }
        when 6

          state = {
            state...
            cards: cardsPositions[6]
          }
        when 7
          analytics.track 'User sent a qustion'

          state = {
            state...
            cards: cardsPositions[7]
          }
        when 15
          analytics.track 'User viewed the Testimonials page'

          state = {
            state...
            cards: cardsPositions[15]
          }
        when 16
          analytics.track 'User viewed the Terms of Service page'

          state = {
            state...
            cards: cardsPositions[16]
          }
        when 20
          analytics.track 'User received and looked at his Advice'

          state = {
            state...
            cards: cardsPositions[20]
          }
        else
          state = {
            state...
            cards: cardsPositions[1]
          }

    else state
