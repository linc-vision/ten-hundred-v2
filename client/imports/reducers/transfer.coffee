export default Transfer = (transfer, props) ->
  loadUser: ->
    transfer
      type: 'LOAD_USER'

  createSession: ->
    transfer
      type: 'CREATE_SESSION'

  showAdviceCard: (advice) ->
    transfer
      type: 'SHOW_ADVICE_CARD'
      payload: advice

  changeCardsPosition: (data) ->
    transfer
      type: 'CHANGE_CARDS_POSITION'
      payload: data
