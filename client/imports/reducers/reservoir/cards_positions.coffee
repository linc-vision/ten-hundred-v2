export default cardsPositions =

  '1':
    state: 1
    active_card: 'blue'
    cold:
      position: 'translate3d(150px, 80px, 0) rotate(-5deg)'
      width: '90%'
      height: 'auto'
      zIndex: 1
      content: 'empty'
    warm:
      position: 'translate3d(-70px, -50px, 0) rotate(-7deg)'
      width: '85%'
      height: 'auto'
      zIndex: 2
      content: 'empty'
    blue:
      position: 'translate3d(-50%, -50%, 0)'
      width: '110%'
      height: '105%'
      zIndex: 3
      content: 'get_advice'

  '2':
    state: 2
    active_card: 'warm'
    cold:
      position: 'translate3d(-12%, 21%, 0) rotate(5deg)'
      width: '90%'
      height: 'auto'
      zIndex: 1
      content: 'empty'
    warm:
      position: 'translate3d(-4%, 0, 0) rotate(0)'
      width: '90%'
      height: 'auto'
      zIndex: 3
      content: 'type_email'
    blue:
      position: 'translate3d(36%, -40%, 0) rotate(32deg) scale(.85)'
      width: '110%'
      height: '110%'
      zIndex: 2
      content: 'rocket_only'

  '3':
    state: 3
    active_card: 'warm'
    cold:
      position: 'translate3d(-12%, 21%, 0) rotate(5deg)'
      width: '90%'
      height: 'auto'
      zIndex: 1
      content: 'empty'
    warm:
      position: 'translate3d(-4%, 0, 0) rotate(0)'
      width: '90%'
      height: 'auto'
      zIndex: 3
      content: 'check_verification_code'
    blue:
      position: 'translate3d(36%, -40%, 0) rotate(32deg) scale(.85)'
      width: '110%'
      height: '110%'
      zIndex: 2
      content: 'rocket_only'

  '4':
    state: 4
    active_card: 'cold'
    cold:
      position: 'translate3d(12%, 0, 0) rotate(0)'
      width: '85%'
      height: 'auto'
      zIndex: 3
      content: 'credit_card'
    warm:
      position: 'translate3d(-95%, 35%, 0) rotate(15deg)'
      width: '90%'
      height: 'auto'
      zIndex: 1
      content: 'letter_only'
    blue:
      position: 'translate3d(36%, -40%, 0) rotate(32deg) scale(.85)'
      width: '110%'
      height: '110%'
      zIndex: 2
      content: 'rocket_only'

  '5':
    state: 5
    active_card: 'cold'
    cold:
      position: 'translate3d(12%, 0, 0) rotate(0)'
      width: '85%'
      height: '110%'
      zIndex: 3
      content: 'question'
    warm:
      position: 'translate3d(-95%, 35%, 0) rotate(15deg)'
      width: '90%'
      height: 'auto'
      zIndex: 1
      content: 'letter_only'
    blue:
      position: 'translate3d(36%, -40%, 0) rotate(32deg) scale(.85)'
      width: '110%'
      height: '110%'
      zIndex: 2
      content: 'rocket_only'

  '6':
    state: 5
    active_card: 'cold'
    cold:
      position: 'translate3d(12%, 0, 0) rotate(0)'
      width: '85%'
      height: '110%'
      zIndex: 3
      content: 'review'
    warm:
      position: 'translate3d(-95%, 35%, 0) rotate(15deg)'
      width: '90%'
      height: 'auto'
      zIndex: 1
      content: 'letter_only'
    blue:
      position: 'translate3d(36%, -40%, 0) rotate(32deg) scale(.85)'
      width: '110%'
      height: '110%'
      zIndex: 2
      content: 'rocket_only'

  '7':
    state: 7
    active_card: 'cold'
    cold:
      position: 'translate3d(12%, 0, 0) rotate(0)'
      width: '85%'
      height: 'auto'
      zIndex: 3
      content: 'complete'
    warm:
      position: 'translate3d(-95%, 35%, 0) rotate(15deg)'
      width: '90%'
      height: 'auto'
      zIndex: 1
      content: 'letter_only'
    blue:
      position: 'translate3d(36%, -40%, 0) rotate(32deg) scale(.85)'
      width: '110%'
      height: '110%'
      zIndex: 2
      content: 'rocket_only'

  '15':
    state: 15
    active_card: 'testimonials'
    cold:
      position: 'translate3d(70%, 160%, 0px) rotate(-7deg)'
      width: '85%'
      height: 'auto'
      zIndex: 1
      content: 'copyright_top'
    warm:
      position: 'translate3d(-113%, 0, 0) rotate(75deg)'
      width: '90%'
      height: 'auto'
      zIndex: 2
      content: 'empty'
    blue:
      position: 'translate3d(36%, -40%, 0) rotate(32deg) scale(.85)'
      width: '110%'
      height: '110%'
      zIndex: 3
      content: 'rocket_only'

  '16':
    state: 16
    active_card: 'terms'
    cold:
      position: 'translate3d(70%, 160%, 0px) rotate(-7deg)'
      width: '85%'
      height: 'auto'
      zIndex: 1
      content: 'copyright_top'
    warm:
      position: 'translate3d(-113%, 0, 0) rotate(75deg)'
      width: '90%'
      height: 'auto'
      zIndex: 2
      content: 'empty'
    blue:
      position: 'translate3d(36%, -40%, 0) rotate(32deg) scale(.85)'
      width: '110%'
      height: '110%'
      zIndex: 3
      content: 'rocket_only'

  '20':
    state: 20
    active_card: 'advice'
    cold:
      position: 'translate3d(10%, 160%, 0) rotate(-3deg)'
      width: '85%'
      height: 'auto'
      zIndex: -3
      content: 'copyright_top'
    warm:
      position: 'translate3d(-16%, -60px, 0) rotate(-5deg)'
      width: '80%'
      height: 'auto'
      zIndex: -2
      content: 'empty'
    blue:
      position: 'translate3d(-36%, -160px, 0) rotate(4deg) scale(.85)'
      width: '110%'
      height: '110%'
      zIndex: -1
      content: 'rocket_only'
