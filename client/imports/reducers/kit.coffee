import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import app from './app'
import admin from './admin'



export default (history) ->
  combineReducers
    router: connectRouter(history)
    app: app
    admin: admin
