Sessions = new Mongo.Collection 'sessions'
Testimonials = new Mongo.Collection 'testimonials'



export default Tracker = ->
  getSessions = Meteor.subscribe 'sessions'
  getTestimonials = Meteor.subscribe 'testimonials'



  tracker:
    userLoaded: if Meteor.userId() and Meteor.user() is undefined then no else yes
    sessionsLoaded: getSessions.ready()
    testimonialsLoaded: getTestimonials.ready()



  sessions: Sessions.find({}, { sort: { createdAt: -1 }}).fetch()
  testimonials: Testimonials.find({}, { sort: { createdAt: -1 }}).fetch()
