import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'



MobileWarmCard = class extends Component
  constructor: (props) ->
    super props
    @state =
      content: @props.app.cards.warm.content
      email_placeholder: [ "T", "Ty", "Typ", "Type", "Type ", "Type y", "Type yo", "Type you", "Type your", "Type your ", "Type your e", "Type your em", "Type your ema", "Type your emai", "Type your email", "Type your email ", "Type your email h", "Type your email he", "Type your email her", "Type your email here", "Type your email here.", "Type your email here..", "Type your email here..." ]
      email_value: ""
      verification_code:
        first: ""
        second: ""
        third: ""
        value: ""
      resendCode:
        resended: no
        value: 0
      completeState: {}



  componentWillReceiveProps: (newProps) =>
    if newProps.app.cards.state is 2
      $('#MobileWarmCard').transition
        transform: 'translate3d(-50%, -50%, 0)'
      , 600, 'ease-out'
    else if newProps.app.cards.state is 4
      $('#MobileWarmCard').transition
        transform: 'translate3d(-160%, -50%, 0)'
      , 300, 'ease-out'

    setTimeout () =>
      @setState
        email_value: ""
        verification_code:
          first: ""
          second: ""
          third: ""
          value: ""
        completeState: {}
    , 800

    if newProps.app.cards.warm.content is 'type_email'
      setTimeout () =>
        @setState
          content: newProps.app.cards.warm.content
        , =>
          setTimeout () =>
            $('#MobileWarmCard #type_email').removeClass 'hidden'
          , 300
          setTimeout () =>
            @state.email_placeholder.map (text, number) =>
              setTimeout () =>
                $('#MobileWarmCard #type_email input').attr 'placeholder', text
              , 1200 + number * 120
          , 1200
      , 300

    else if newProps.app.cards.warm.content is 'check_verification_code'
      $('#MobileWarmCard #type_email').addClass 'hidden'
      setTimeout () =>
        @setState
          content: newProps.app.cards.warm.content
        , ->
          $('#MobileWarmCard #check_verification_code').removeClass 'hidden'
      , 400



  focusEmail: =>
    $('#App #layout').scrollTop $(document).height()

  typeEmail: (e) =>
    @setState
      email_value: e.target.value

  submitEmail: =>
    Meteor.call 'submitEmail', @props.app.session.id, @state.email_value
    @props.changeCardsPosition 3
    setTimeout () =>
      @setState
        email_value: ""
    , 1000

  focusVerificationCode: (number, e) =>
    if number is 1
      if @state.verification_code.first.length > 0
        @setState
          verification_code: {
            @state.verification_code...
            first: ""
            value: "#{ @state.verification_code.second }#{ @state.verification_code.third }"
          }
    else if number is 2
      if @state.verification_code.second.length > 0
        @setState
          verification_code: {
            @state.verification_code...
            second: ""
            value: "#{ @state.verification_code.first }#{ @state.verification_code.third }"
          }
    else if number is 3
      if @state.verification_code.third.length > 0
        @setState
          verification_code: {
            @state.verification_code...
            third: ""
            value: "#{ @state.verification_code.first }#{ @state.verification_code.second }"
          }

  typeVerificationCode: (digit, e) =>
    if digit is 1
      @setState
        verification_code: {
          @state.verification_code...
          first: e.target.value
        }
      , =>
        @setState
          verification_code: {
            @state.verification_code...
            value: "#{ @state.verification_code.first }#{ @state.verification_code.second }#{ @state.verification_code.third }"
          }
        , =>
          if @state.verification_code.second.length is 0
            $('#MobileWarmCard #check_verification_code #input input').eq(1).focus()
          else
            $('#MobileWarmCard #check_verification_code #input input').eq(0).blur()
    else if digit is 2
      @setState
        verification_code: {
          @state.verification_code...
          second: e.target.value
        }
      , =>
        @setState
          verification_code: {
            @state.verification_code...
            value: "#{ @state.verification_code.first }#{ @state.verification_code.second }#{ @state.verification_code.third }"
          }
        , =>
          if @state.verification_code.third.length is 0
            $('#MobileWarmCard #check_verification_code #input input').eq(2).focus()
          else
            $('#MobileWarmCard #check_verification_code #input input').eq(1).blur()
    else if digit is 3
      @setState
        verification_code: {
          @state.verification_code...
          third: e.target.value
        }
      , =>
        @setState
          verification_code: {
            @state.verification_code...
            value: "#{ @state.verification_code.first }#{ @state.verification_code.second }#{ @state.verification_code.third }"
          }
        , =>
          $('#MobileWarmCard #check_verification_code #input input').eq(2).blur()

  resendVerificationCode: =>
    Meteor.call 'resendVerificationCode', @props.app.session.id, (err, res) =>
      if not err
        @setState
          resendCode:
            resended: yes
            value: 60
        , =>
          minute = setInterval () =>
            @setState
              resendCode: {
                ...@state.resendCode
                value: @state.resendCode.value - 1
              }
            , =>
              if @state.resendCode.value is 0
                @setState
                  resendCode:
                    resended: no
                    value: 60
                clearInterval minute
          , 1000

  checkVerificationCode: =>
    if @state.completeState.code is 'S'
      @props.changeCardsPosition 4
    else
      Meteor.call 'checkVerificationCode', @props.app.session.id, @state.verification_code.value, (err, res) =>
        if not err
          $('#MobileWarmCard #check_verification_code').addClass 'hidden'
          @showMessage res

  showMessage: (res) =>
    setTimeout () =>
      @setState
        verification_code:
          first: ""
          second: ""
          third: ""
          value: ""
        completeState: res
      , =>
        setTimeout () =>
          $('#MobileWarmCard #check_verification_code').removeClass 'hidden'
        , 400
    , 400



  render: =>
    <div id='MobileWarmCard' className='warm_card'>
      <div id='layout'>
        {
          if @state.content is 'type_email'
            <div id='type_email' className='hidden'>
              <div id='layout'>
                <div id='top'>
                  <img src='./img/pictures/letter_dark_full.png' alt='Ten Hundred letter'/>
                </div>
                <div id='content'>
                  <div id='title'>
                    <h3>Email address</h3>
                  </div>
                  <div id='text'>
                    <p>Great to see you here, we can’t wait to hear from you! First, enter your email.</p>
                  </div>
                  <div id='input'>
                    <input type='text' name='email' placeholder='' value={ @state.email_value } onChange={ @typeEmail } onFocus={ @focusEmail }/>
                  </div>
                </div>
                <div id='next' className={ if @state.email_value.length > 5 and @state.content is 'type_email' then 'big_button' else 'big_button hidden' } onClick={ if @state.email_value.length > 5 and @state.content is 'type_email' then @submitEmail }>
                  <div id='icon'></div>
                  <p>NEXT</p>
                </div>
              </div>
            </div>
          else if @state.content is 'check_verification_code'
            <div id='check_verification_code' className='hidden'>
              <div id='layout'>
                <div id='top'>
                  <img src='./img/pictures/letter_dark_full.png' alt='Ten Hundred letter'/>
                </div>
                <div id='content'>
                  {
                    if @state.completeState.code is 'F'
                      <div id='title'>
                        <h3 className='warning'>{ @state.completeState.message }</h3>
                        <p onClick={ @props.changeCardsPosition.bind this, 2 }>or re-enter email address</p>
                      </div>
                    else if @state.completeState.code is 'S'
                      <div id='title'>
                        <h3>{ @state.completeState.message }</h3>
                      </div>
                    else
                      <div id='title'>
                        <h3 className='opacity_6'>We just sent you the confirmation email. Please check your inbox.</h3>
                        <p onClick={ @props.changeCardsPosition.bind this, 2 }>or re-enter email address</p>
                      </div>
                  }
                  {
                    if @state.completeState.code isnt 'S'
                      <div id='text'>
                        <h3>Type the confirmation code:</h3>
                      </div>
                  }
                  {
                    if @state.completeState.code isnt 'S'
                      <div id='input'>
                        <input type='number' value={ @state.verification_code.first } onChange={ @typeVerificationCode.bind this, 1 } onFocus={ @focusVerificationCode.bind this, 1 }/>
                        <input type='number' value={ @state.verification_code.second } onChange={ @typeVerificationCode.bind this, 2 } onFocus={ @focusVerificationCode.bind this, 2 }/>
                        <input type='number' value={ @state.verification_code.third } onChange={ @typeVerificationCode.bind this, 3 } onFocus={ @focusVerificationCode.bind this, 3 }/>
                        <p id='resendVerificationCode' className={ if @state.resendCode.resended then 'inactive' } onClick={ if not @state.resendCode.resended then @resendVerificationCode }>{ if not @state.resendCode.resended then "Resend code" else "Resended. Try again in #{ @state.resendCode.value }" }</p>
                      </div>
                  }
                </div>
                <div id='next' className={ if @state.verification_code.value.length is 3 and @state.content is 'check_verification_code' or @state.completeState.code is 'S' then 'big_button' else 'big_button hidden' } onClick={ if @state.verification_code.value.length is 3 and @state.content is 'check_verification_code' or @state.completeState.code is 'S' then @checkVerificationCode }>
                  <div id='icon'></div>
                  <p>NEXT</p>
                </div>
              </div>
            </div>
        }
      </div>
    </div>



export default outfit MobileWarmCard
