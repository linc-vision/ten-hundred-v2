import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'


MobileBlueCard = class extends Component
  constructor: (props) ->
    super props
    @state = {}



  componentWillReceiveProps: (newProps) =>
    if newProps.app.cards.state is 1
      $('#MobileBlueCard').transition
        transform: 'translate3d(-50%, -50%, 0) scale(1)'
      , 300, 'ease-out'
    else if newProps.app.cards.state is 2
      $('#MobileBlueCard').transition
        transform: 'translate3d(-150%, -50%, 0) scale(.8)'
      , 300, 'ease-out'
    else if newProps.app.cards.state is 4
      $('#MobileBlueCard').transition
        transform: 'translate3d(-250%, -50%, 0) scale(.8)'
      , 300, 'ease-out'
    else if newProps.app.cards.state is 15
      $('#MobileBlueCard').transition
        transform: 'translate3d(150%, -50%, 0) scale(.8)'
      , 300, 'ease-out'
    else if newProps.app.cards.state is 16
      $('#MobileBlueCard').transition
        transform: 'translate3d(150%, -50%, 0) scale(.8)'
      , 300, 'ease-out'
    else if newProps.app.cards.state is 20
      $('#MobileBlueCard').transition
        transform: 'translate3d(150%, -50%, 0) scale(.8)'
      , 300, 'ease-out'



  render: =>
    <div id='MobileBlueCard' className='blue_card'>
      <div id='layout'>
        <div id='get_advice'>
          <div id='layout'>
            <div id='arounds'></div>
            <div id='top'>
              <img src='./img/pictures/rocket.png' alt='Ten Hundred rocket'/>
            </div>
            <div id='content'>
              <div id='title'>
                <h3>Get advice on anything for</h3><h2>$50</h2>
                <p>(You won’t be charged until your question is answered)</p>
              </div>
              <div id='text'>
                <p>Not sure where to turn to get advice? Ten Hundred is a convenient platform for you to get confidential, personal, and empathetic guidance — love and relationships, family and child-rearing, career and self-improvement, and much more…</p>
              </div>
              <div id='button'>
                <button className='small_button' onClick={ @props.changeCardsPosition.bind this, 2 }>Get Advice</button>
              </div>
              <p id='terms' onClick={ @props.changeCardsPosition.bind this, 16 }>Terms of Service</p>
            </div>
          </div>
        </div>
        {
          if @props.testimonials.length > 0
            <Route exact path='/' render={ () =>
              <div id='testimonials_button'>
                <div id='layer_big'></div>
                <div id='layer_small'></div>
                <div className='big_button' onClick={ @props.changeCardsPosition.bind this, 15 }>
                  <div id='icon'></div>
                  <p>Testimonials</p>
                </div>
              </div>
            }/>
        }
      </div>
    </div>



export default outfit MobileBlueCard
