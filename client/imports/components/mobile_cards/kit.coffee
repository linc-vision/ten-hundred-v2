import React, { Component } from 'react'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'

import MobileBlueCard from './components/mobile_blue_card/kit'
import MobileWarmCard from './components/mobile_warm_card/kit'
import MobileColdCard from './components/mobile_cold_card/kit'
import MobileAdviceCard from './components/mobile_advice_card/kit'
import MobileTestimonials_list from './components/mobile_testimonials_list/kit'
import MobileTerms from './components/mobile_terms/kit'


MobileCards = class extends Component
  constructor: (props) ->
    super props
    @state = {}



  componentDidMount: =>
    setTimeout () =>
      $('#App > #layout > .mobile_cards_layout').removeClass 'hidden'
      setTimeout () =>
        $('#App > #layout > .mobile_cards_layout').css
          transition: 'none'
      , 400
    , 100

    if @props.location.search isnt ''
      Meteor.call 'getAdvice', @props.location.search, (err, res) =>
        if err
          @props.history.push ''
          @props.createSession()
        else
          if res.code is 'S'
            @props.changeCardsPosition 20
            @props.showAdviceCard res.advice
          else
            @props.history.push ''
            @props.createSession()
    else
      @props.createSession()



  render: =>
    <div className='mobile_cards_layout hidden'>
      <div id='header'>
        <img src='./img/pictures/logo_title.png' alt='Ten Hundred Logo'/>
      </div>
      <MobileBlueCard />
      <MobileWarmCard />
      <MobileColdCard />
      {
        if @props.testimonials.length > 0
          <MobileTestimonials_list />
      }
      <MobileTerms />
      {
        if @props.app.adviceCard
          <MobileAdviceCard />
      }
      <div id='footer'>
        <p>{ "© Ten Hundred #{ new Date().getFullYear() }. All Rights Reserved." }</p>
      </div>
    </div>



export default outfit MobileCards
