import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'

import Login from './components/login/kit'
import Dashboard from './components/dashboard/kit'
import NotAdmin from './components/not_admin/kit'



Admin = class extends Component
  constructor: (props) ->
    super props
    @state = {}



  render: =>
    <div id='Admin'>
      {
        if @props.app.user
          if @props.tracker.userLoaded
            if @props.app.user.profile.admin
              <Dashboard />
            else
              <NotAdmin />
        else
          <Login />
      }
    </div>



export default outfit Admin
