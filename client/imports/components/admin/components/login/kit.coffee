import React, { Component } from 'react'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'


Login = class extends Component
  constructor: (props) ->
    super props
    @state =
      username: ""
      password: ""
      message: ""



  componentDidMount: =>
    setTimeout () =>
      $('#Login').removeClass 'hidden'
      setTimeout () =>
        $('#Login .warm_card').css
          transition: 'none'
      , 600
    , 100



  typeUsername: (e) =>
    @setState
      username: e.target.value.replace(/[^A-z-_.@]/g, '')

  typePassword: (e) =>
    @setState
      password: e.target.value

  signIn: =>
    Meteor.loginWithPassword @state.username, @state.password, (err, res) =>
      if err
        @showMessage "Invalid username or password"
      else
        $('#Login .warm_card').css
          transition: '.4s'
        $('#Login').addClass 'hidden'
        setTimeout () =>
          @props.loadUser()
        , 600

  showMessage: (message) =>
    @setState
      message: message
      password: ""
    , =>
      $('#Login #message').removeClass 'hidden'
      setTimeout () =>
        $('#Login #message').addClass 'hidden'
        setTimeout () =>
          @setState
            message: ""
        , 300
      , 5000




  render: =>
    <div id='Login' className='hidden'>
      <div className='cards_layout'>
        <div className='warm_card active_card'>
          <div id='layout'>
            <div id='title'>
              <h3>Admin login</h3>
            </div>
            <div id='input'>
              <input type='text' name='email' placeholder='Username' value={ @state.username } onChange={ @typeUsername }/>
              <input type='password' name='password' placeholder='Password' value={ @state.password } onChange={ @typePassword }/>
            </div>
            <p id='message' className='hidden'>{ @state.message }</p>
            <div id='next' className={ if @state.username.length > 5 and @state.password.length > 5 then 'big_button' else 'big_button hidden' } onClick={ if @state.username.length > 5 and @state.password.length > 5 then @signIn }>
              <div id='icon'></div>
              <p>NEXT</p>
            </div>
          </div>
        </div>
      </div>
    </div>



export default outfit Login
