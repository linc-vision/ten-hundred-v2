import React, { Component } from 'react'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'

import Testimonials from './components/testimonials/kit'
import Answer from './components/answer/kit'



Dashboard = class extends Component
  constructor: (props) ->
    super props
    @state =
      content: 'dashboard'
      sessionsCount: () =>
        sessions = @props.sessions.filter (item) => not item.removed
        sessions.length
      earned: () =>
        paidSessions = @props.sessions.filter (item) => item.paid
        if paidSessions.length > 0
          parseFloat paidSessions.length * (50 - 2.9 / 2 - 0.30), 2
        else
          0
      answerTo: ''



  componentDidMount: =>
    setTimeout () =>
      $('#Dashboard').removeClass 'hidden'
    , 100

  componentWillReceiveProps: (newProps) =>
    if @props.sessions isnt newProps.sessions
      @setState
        sessionsCount: () =>
          sessions = @props.sessions.filter (item) => not item.removed
          sessions.length
        earned: () =>
          paidSessions = newProps.sessions.filter (item) => item.paid
          parseFloat paidSessions.length * (50 - 2.9 / 2 - 0.30), 2



  showMore: (e) =>
    sibling = $(e.target).siblings('#text')

    if sibling.hasClass 'short'
      $(e.target).text 'hide'
      sibling.removeClass 'short'
    else
      $(e.target).text 'show more'
      sibling.addClass 'short'

  removeSession: (id) =>
    $("#Dashboard #sessions_list .item##{ id }").hide 200, =>
      Meteor.call 'removeSession', id

  openAnswer: (item) =>
    @setState
      content: 'answer'
      answerTo: item

  closeAnswer: =>
    $('#Admin #Dashboard #Answer').addClass 'hidden'
    setTimeout () =>
      @setState
        content: 'dashboard'
        answerTo: ''
    , 100

  sendAnswer: (id) =>
    Meteor.call 'sendAnswer', id

  resendAnswer: (id) =>
    Meteor.call 'resendAnswer', id

  openTestimonials: =>
    @setState
      content: 'testimonials'

  closeTestimonials: =>
    $('#Admin #Dashboard #Testimonials').addClass 'hidden'
    setTimeout () =>
      @setState
        content: 'dashboard'
    , 100

  logout: =>
    Meteor.logout (err, res) =>
      if not err
        $('#Dashboard').addClass 'hidden'
        setTimeout () =>
          @props.loadUser()
        , 400



  render: =>
    <div id='Dashboard' className='hidden'>
      <div id='layout'>
        <div id='sessions_list' className={ if @state.content is 'testimonials' or @state.content is 'answer' then 'hidden' } onClick={ if @state.content is 'testimonials' then @closeTestimonials else if @state.content is 'answer' then @closeAnswer }>
          <div id='list'>
            {
              if @state.sessionsCount() > 0
                @props.sessions.map (item, number) =>
                  if not item.removed
                    <div key={ number } id={ item.id } className='item blue_card'>
                      <div id='layout'>
                        <div id='email'>
                          <p id='label'>Email</p>
                          <p>{ item.email }</p>
                        </div>
                        <div id='info'>
                          <div id='left'>
                            <p id='marker' className={ if item.payment_method_verified then 'yes' else 'no' }>{ if item.payment_method_verified then 'yes' else 'no' }</p>
                            <p id='label'>Verified</p>
                          </div>
                          <div id='center'>
                            <p id='marker' className={ if item.answered then 'yes' else 'no' }>{ if item.answered then 'yes' else 'no' }</p>
                            <p id='label'>Answered</p>
                          </div>
                          <div id='right'>
                            <p id='marker' className={ if item.sended then 'yes' else 'no' }>{ if item.sended then 'yes' else 'no' }</p>
                            <p id='label'>Sent</p>
                          </div>
                        </div>
                        <div id='question'>
                          <p id='label'>Question</p>
                          <p id='text' className='short'>{ item.question }</p>
                          <p id='show_more' onClick={ @showMore }>show more</p>
                        </div>
                        <div id='panel'>
                          <div id='left'>
                            {
                              if not item.payment_method_verified and not item.answered and not item.sended or item.payment_method_verified and item.answered and item.sended
                                <p id='remove' onClick={ @removeSession.bind this, item.id, number }>Remove</p>
                            }
                          </div>
                          <div id='center'>
                            {
                              if item.payment_method_verified and not item.sended
                                <button className='small_button' onClick={ @openAnswer.bind this, item }>{ if item.answered then 'Edit Answer' else 'Answer' }</button>
                            }
                          </div>
                          <div id='right'>
                            {
                              if item.payment_method_verified and item.answered
                                <button className='small_button cold' onClick={ if not item.sended then @sendAnswer.bind this, item.id else @resendAnswer.bind this, item.id }>{ if not item.sended then 'Send' else 'Resend' }</button>
                            }
                          </div>
                        </div>
                      </div>
                    </div>
              else
                <div id='empty' className='item blue_card'>
                  <div id='layout'>
                    <h1>There are no sessions yet.</h1>
                  </div>
                </div>
            }
          </div>
        </div>
        <div id='sidebar' className={ if @state.content is 'testimonials' or @state.content is 'answer' then 'hidden' } onClick={ if @state.content is 'testimonials' then @closeTestimonials else if @state.content is 'answer' then @closeAnswer }>
          <div id='layout'>
            <div id='first' className='warm_card'>
              <div id='layout'>
                <div id='bg'></div>
                <div id='content'>
                  <h1>{ @state.sessionsCount() }</h1>
                  <h3>Sessions</h3>
                </div>
              </div>
            </div>
            <div id='second' className='cold_card'>
              <div id='layout'>
                <div id='bg'></div>
                <div id='content'>
                  <h1>{ "$#{ @state.earned() }" }</h1>
                  <h3>Earned</h3>
                </div>
              </div>
            </div>
            <div id='third' className='blue_card'>
              <div id='layout'>
                <div id='content'>
                  <button className='small_button' onClick={ @openTestimonials }>Add Testimonial</button>
                  <p onClick={ @logout }>Log out</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {
          if @state.content is 'testimonials'
            <Testimonials close={ @closeTestimonials }/>
          else if @state.content is 'answer'
            <Answer close={ @closeAnswer } session={ @state.answerTo }/>
        }
      </div>
    </div>



export default outfit Dashboard
