import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'



Testimonials = class extends Component
  constructor: (props) ->
    super props
    @state =
      content: 'testimonials'
      author: ""
      testimonial: ""



  componentDidMount: =>
    setTimeout () =>
      $('#Admin #Dashboard #Testimonials').removeClass 'hidden'
    , 100



  back: =>
    if @state.content is 'testimonials'
      @props.close()
    else
      @setState
        content: 'testimonials'
        author: ""
        testimonial: ""

  createTestimonial: =>
    @setState
      content: 'type_testimonial'

  typeAuthor: (e) =>
    @setState
      author: e.target.value

  typeTestimonial: (e) =>
    @setState
      testimonial: e.target.value

  addTestimonial: =>
    Meteor.call 'addTestimonial', { author: @state.author, testimonial: @state.testimonial }, (err, res) =>
      if not err
        @setState
          content: 'testimonials'
          author: ""
          testimonial: ""

  removeTestimonial: (id) =>
    Meteor.call 'removeTestimonial', id



  render: =>
    <div id='Testimonials' className='hidden'>
      <div id='layout'>
        <div id='testimonials_list'>
          {
            if @state.content is 'type_testimonial'
              <div id='newTestimonial' className='warm_card'>
                <div id='layout'>
                  <div id='author'>
                    <p id='label'>Author</p>
                    <input type='text' name='author' value={ @state.author } placeholder='Type the testimonial author name or company name...' onChange={ @typeAuthor }/>
                  </div>
                  <div id='testimonial'>
                    <p id='label'>Testimonial</p>
                    <textarea name='testimonial' rows='8' value={ @state.testimonial } placeholder='Type the testimonial text...' onChange={ @typeTestimonial }/>
                  </div>
                  <div id='panel'>
                    {
                      if @state.testimonial.length > 10
                        <button className='small_button' onClick={ @addTestimonial }>Add testimonial</button>
                    }
                  </div>
                </div>
              </div>
          }
          <div id='list'>
            {
              if @props.testimonials.length > 0
                @props.testimonials.map (item, number) =>
                  <div key={ item._id } id={ item._id } className='item warm_card'>
                    <div id='layout'>
                      {
                        if item.author.length > 0
                          <div id='author'>
                            <p id='label'>Author</p>
                            <p>{ item.author }</p>
                          </div>
                      }
                      <div id='testimonial'>
                        <p id='label'>Testimonial</p>
                        <p id='text'>{ item.testimonial }</p>
                      </div>
                      <div id='panel'>
                        <div id='left'>
                          <p id='remove' onClick={ @removeTestimonial.bind this, item._id }>Remove</p>
                        </div>
                      </div>
                    </div>
                  </div>
              else
                <div id='empty' className='item blue_card'>
                  <div id='layout'>
                    <h1>There are no testimonials yet.</h1>
                  </div>
                </div>
            }
          </div>
        </div>
        <div id='back' className='big_button' onClick={ @back }>
          <div id='icon'></div>
          <p>BACK</p>
        </div>
        <div id='add' className={ if @state.content is 'testimonials' then 'big_button warm' else 'big_button warm hidden' } onClick={ @createTestimonial }>
          <div id='icon'></div>
          <p>ADD</p>
        </div>
      </div>
    </div>



export default outfit Testimonials
