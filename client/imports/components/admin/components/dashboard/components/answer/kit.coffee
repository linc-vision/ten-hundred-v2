import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'



Answer = class extends Component
  constructor: (props) ->
    super props
    @state =
      title: ""
      subtitle: ""
      text: ""



  componentDidMount: =>
    if @props.session.answered
      @setState
        title: @props.session.answer.title
        subtitle: @props.session.answer.subtitle
        text: @props.session.answer.text
    setTimeout () =>
      $('#Admin #Dashboard #Answer').removeClass 'hidden'
    , 100



  typeTitle: (e) =>
    @setState
      title: e.target.value

  typeSubtitle: (e) =>
    @setState
      subtitle: e.target.value

  typeAnswer: (e) =>
    @setState
      text: e.target.value

  saveAnswer: =>
    data =
      title: @state.title
      subtitle: @state.subtitle
      text: @state.text

    Meteor.call 'saveAnswer', @props.session.id, data, (err, res) =>
      if not err
        @props.close()



  render: =>
    <div id='Answer' className='hidden'>
      <div id='layout'>
        <div id='answer' className='item warm_card'>
          <div id='layout'>
            <div id='email'>
              <p id='label'>Email</p>
              <p>{ @props.session.email }</p>
            </div>
            <div className='input'>
              <p id='label'>Title</p>
              <input name='title' placeholder='Type an answer title...' value={ @state.title } onChange={ @typeTitle }/>
            </div>
            <div className='input'>
              <p id='label'>Subtitle</p>
              <input name='subtitle' placeholder='Type some subtitle...' value={ @state.subtitle } onChange={ @typeSubtitle }/>
            </div>
            <div className='input'>
              <p id='label'>Answer</p>
              <textarea name='text' rows='12' placeholder='Type an answer text...' value={ @state.text } onChange={ @typeAnswer }/>
            </div>
            <div id='question'>
              <p id='label'>Question</p>
              <p id='text'>{ @props.session.question }</p>
            </div>
          </div>
        </div>
        <div id='back' className='big_button' onClick={ @props.close }>
          <div id='icon'></div>
          <p>BACK</p>
        </div>
        <div id='add' className={ if @state.title.length > 2 and @state.subtitle.length > 2 and @state.text.length > 20 then 'big_button warm' else 'big_button warm hidden' } onClick={ if @state.title.length > 2 and @state.subtitle.length > 2 and @state.text.length > 20 then @saveAnswer }>
          <div id='icon'></div>
          <p>ANSWER</p>
        </div>
      </div>
    </div>



export default outfit Answer
