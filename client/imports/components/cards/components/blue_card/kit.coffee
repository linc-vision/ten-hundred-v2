import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'


BlueCard = class extends Component
  constructor: (props) ->
    super props
    @state = {}



  componentWillReceiveProps: (newProps) =>
    if newProps.app.cards.state isnt @props.app.cards.state
      $('#BlueCard').transition
        transform: newProps.app.cards.blue.position
        width: newProps.app.cards.blue.width
        height: newProps.app.cards.blue.height
      , 600, 'ease-in-out', ->
        $('#BlueCard').css
          zIndex: newProps.app.cards.blue.zIndex



  render: =>
    <div id='BlueCard' className={ if @props.app.cards.active_card is 'blue' then 'blue_card active_card' else 'blue_card' }>
      <div id='layout'>
        <div id='get_advice'>
          <div id='layout'>
            <div id='arounds'></div>
            <div id='left'>
              <img src='./img/pictures/rocket.png' alt='Ten Hundred rocket'/>
            </div>
            <div id='right' className={ if @props.app.cards.blue.content is 'rocket_only' then 'hidden' else '' }>
              <div id='title'>
                <h3>Get advice on anything for</h3><h2>$50</h2>
                <p>(You won’t be charged until your question is answered)</p>
              </div>
              <div id='text'>
                <p>Not sure where to turn to get advice? Ten Hundred is a convenient platform for you to get confidential, personal, and empathetic guidance — love and relationships, family and child-rearing, career and self-improvement, and much more…</p>
              </div>
              <div id='button'>
                <button className='small_button' onClick={ @props.changeCardsPosition.bind this, 2 }>Get Advice</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



export default outfit BlueCard
