import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'



Terms = class extends Component
  constructor: (props) ->
    super props
    @state = {}



  back: =>
    @props.changeCardsPosition 1



  render: =>
    <div id='Terms' className={ if @props.app.cards.active_card is 'terms' then 'active_card' else 'hidden' }>
      <div id='layout'>
        <div id='terms_of_service'>
          <p><strong>Terms of Service: Please read below before you submit your question</strong></p>
          <br/>
          <p>TenHundred.com responds to each and every request, but we are <strong>PROHIBITED</strong> from offering a few types of replies:</p>
          <ul>
            <li>ANY type of question that requires <strong>medical, tax, legal or investment</strong> opinion or advice of any nature is against our corporate charter and by-laws and cannot be answered.</li>
            <li>Our staff are primarily based in the United States; we will not be able to answer letters that require a comprehensive understanding of <strong>cultural norms, career or educational structures outside of the US.</strong></li>
            <li>Questions submitted with <strong>insufficient information or detail</strong> to allow a proper reply cannot be answered.</li>
            <li>Obscure questions that require <strong>specialized knowledge not likely held by the general American population</strong> cannot be answered.</li>
            <li>Requests to assist with <strong>homework, projects, business plans or similar tasks</strong> cannot be answered.</li>
          </ul>
          <br/>
          <p><strong>Refund Policy</strong></p>
          <p>Please be advised that all purchases are final and no refund will be given. We encourage you to read through the testimonials to decide if our service is right for you before placing a request.</p>
        </div>
        <div id='back' className='big_button' onClick={ @back }>
          <div id='icon'></div>
          <p>BACK</p>
        </div>
      </div>
    </div>



export default outfit Terms
