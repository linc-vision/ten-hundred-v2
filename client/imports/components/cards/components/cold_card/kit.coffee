import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'


ColdCard = class extends Component
  constructor: (props) ->
    super props
    @state =
      content: @props.app.cards.cold.content
      card_typing_count: 0
      card_number:
        first: ""
        second: ""
        third: ""
        fourth: ""
        value: ""
      exp_date:
        first: ""
        second: ""
        value: ""
      cvv: ""
      name: ""
      zipcode: ""
      payment_notification: ""
      card_submitted: no
      question: ""
      question_typing_count: 0



  componentWillReceiveProps: (newProps) =>
    if newProps.app.cards.state isnt @props.app.cards.state
      $('#ColdCard').transition
        transform: newProps.app.cards.cold.position
        width: newProps.app.cards.cold.width
        height: newProps.app.cards.cold.height
      , 600, 'ease-in-out', ->
        $('#ColdCard').css
          zIndex: newProps.app.cards.cold.zIndex

    setTimeout () =>
      @setState
        card_number:
          first: ""
          second: ""
          third: ""
          fourth: ""
          value: ""
        exp_date:
          first: ""
          second: ""
          value: ""
        cvv: ""
    , 800

    if newProps.app.cards.cold.content is 'credit_card'
      setTimeout () =>
        @setState
          content: newProps.app.cards.cold.content
        , =>
          setTimeout () =>
            $('#ColdCard #credit_card').removeClass 'hidden'
          , 800
      , 600

    else if newProps.app.cards.cold.content is 'question'
      $('#ColdCard #credit_card').addClass 'hidden'
      setTimeout () =>
        @setState
          content: newProps.app.cards.cold.content
        , =>
          setTimeout () =>
            $('#ColdCard #question').removeClass 'hidden'
          , 400
      , 400

    else if newProps.app.cards.cold.content is 'review'
      $('#ColdCard #question').addClass 'hidden'
      setTimeout () =>
        @setState
          content: newProps.app.cards.cold.content
        , =>
          setTimeout () =>
            $('#ColdCard #review').removeClass 'hidden'
          , 400
      , 400

    else if newProps.app.cards.cold.content is 'complete'
      $('#ColdCard #review').addClass 'hidden'
      setTimeout () =>
        @setState
          content: newProps.app.cards.cold.content
        , =>
          setTimeout () =>
            $('#ColdCard #complete').removeClass 'hidden'
          , 400
      , 400



  focusCardNumber: (number, e) =>
    if number is 1
      if @state.card_number.first.length > 0
        @setState
          card_number: {
            @state.card_number...
            first: ""
            value: "#{ @state.card_number.second }#{ @state.card_number.third }#{ @state.card_number.fourth }"
          }
    else if number is 2
      if @state.card_number.second.length > 0
        @setState
          card_number: {
            @state.card_number...
            second: ""
            value: "#{ @state.card_number.first }#{ @state.card_number.third }#{ @state.card_number.fourth }"
          }
    else if number is 3
      if @state.card_number.third.length > 0
        @setState
          card_number: {
            @state.card_number...
            third: ""
            value: "#{ @state.card_number.first }#{ @state.card_number.second }#{ @state.card_number.fourth }"
          }
    else if number is 4
      if @state.card_number.fourth.length > 0
        @setState
          card_number: {
            @state.card_number...
            fourth: ""
            value: "#{ @state.card_number.first }#{ @state.card_number.second }#{ @state.card_number.third }"
          }

  typeCardNumber: (number, e) =>
    if @state.card_typing_count is 0
      analytics.track 'User started typing credit card data'

    @setState
      card_typing_count: @state.card_typing_count + 1

    if number is 1
      @setState
        card_number: {
          @state.card_number...
          first: e.target.value.replace(/[^0-9]/g, '')
        }
      , =>
        @setState
          card_number: {
            @state.card_number...
            value: "#{ @state.card_number.first }#{ @state.card_number.second }#{ @state.card_number.third }#{ @state.card_number.fourth }"
          }
        , =>
          if @state.card_number.first.length is 4
            if @state.card_number.second.length is 0
              $('#ColdCard #card_number #input input').eq(1).focus()
            else
              $('#ColdCard #card_number #input input').eq(0).blur()
    else if number is 2
      @setState
        card_number: {
          @state.card_number...
          second: e.target.value.replace(/[^0-9]/g, '')
        }
      , =>
        @setState
          card_number: {
            @state.card_number...
            value: "#{ @state.card_number.first }#{ @state.card_number.second }#{ @state.card_number.third }#{ @state.card_number.fourth }"
          }
        , =>
          if @state.card_number.second.length is 4
            if @state.card_number.third.length is 0
              $('#ColdCard #card_number #input input').eq(2).focus()
            else
              $('#ColdCard #card_number #input input').eq(1).blur()
    else if number is 3
      @setState
        card_number: {
          @state.card_number...
          third: e.target.value.replace(/[^0-9]/g, '')
        }
      , =>
        @setState
          card_number: {
            @state.card_number...
            value: "#{ @state.card_number.first }#{ @state.card_number.second }#{ @state.card_number.third }#{ @state.card_number.fourth }"
          }
        , =>
          if @state.card_number.third.length is 4
            if @state.card_number.fourth.length is 0
              $('#ColdCard #card_number #input input').eq(3).focus()
            else
              $('#ColdCard #card_number #input input').eq(2).blur()
    else if number is 4
      @setState
        card_number: {
          @state.card_number...
          fourth: e.target.value.replace(/[^0-9]/g, '')
        }
      , =>
        @setState
          card_number: {
            @state.card_number...
            value: "#{ @state.card_number.first }#{ @state.card_number.second }#{ @state.card_number.third }#{ @state.card_number.fourth }"
          }
        , =>
          if @state.card_number.fourth.length is 4
            if @state.exp_date.first.length is 0
              $('#ColdCard #exp_date #input input').eq(0).focus()
            else
              $('#ColdCard #card_number #input input').eq(3).blur()

  focusExpDate: (number, e) =>
    if number is 1
      if @state.exp_date.first.length > 0
        @setState
          exp_date: {
            @state.exp_date...
            first: ""
            value: "#{ @state.exp_date.second }"
          }
    if number is 2
      if @state.exp_date.second.length > 0
        @setState
          exp_date: {
            @state.exp_date...
            second: ""
            value: "#{ @state.exp_date.first }"
          }

  typeExpDate: (number, e) =>
    if number is 1
      @setState
        exp_date: {
          @state.exp_date...
          first: e.target.value.replace(/[^0-9]/g, '')
        }
      , =>
        @setState
          exp_date: {
            @state.exp_date...
            value: "#{ @state.exp_date.first }/#{ @state.exp_date.second }"
          }
        , =>
          if @state.exp_date.first.length is 2
            if @state.exp_date.second.length is 0
              $('#ColdCard #exp_date #input input').eq(1).focus()
            else
              $('#ColdCard #exp_date #input input').eq(0).blur()
    else if number is 2
      @setState
        exp_date: {
          @state.exp_date...
          second: e.target.value.replace(/[^0-9]/g, '')
        }
      , =>
        @setState
          exp_date: {
            @state.exp_date...
            value: "#{ @state.exp_date.first }/#{ @state.exp_date.second }"
          }
        , =>
          if @state.exp_date.second.length is 2
            if @state.cvv.length is 0
              $('#ColdCard #cvv #input input').eq(0).focus()
            else
              $('#ColdCard #exp_date #input input').eq(1).blur()

  focusCvv: =>
    if @state.cvv.length > 0
      @setState
        cvv: ""

  typeCvv: (e) =>
    @setState
      cvv: e.target.value.replace(/[^0-9]/g, '')
    , =>
      if @state.cvv.length is 3
        if @props.app.PAYMENT_SERVICE is 'WePay'
          $('#ColdCard #name #input input').eq(0).focus()
        else
          $('#ColdCard #cvv #input input').eq(0).blur()

  typeName: (e) =>
    @setState
      name: e.target.value.replace(/[^A-z -]/g, '').toUpperCase()

  typeZipcode: (e) =>
    @setState
      zipcode: e.target.value.replace(/[^0-9]/g, '')

  enterCard: (e) =>
    if e.key is 'Enter'
      if @state.card_submitted
        @goToQuestion()
      else if @state.card_number.value.length is 16 and @state.exp_date.value.length >= 4 and @state.cvv.length is 3 and @state.content is 'credit_card'
        @submitCreditCard()

  submitCreditCard: =>
    if not $('#ColdCard #credit_card #next').hasClass 'loading'
      $('#ColdCard #credit_card #next').addClass 'loading'
      $('#ColdCard #credit_card #layer').addClass 'visible'

      if @props.app.PAYMENT_SERVICE is 'WePay'
        data =
          card_number: @state.card_number.value
          exp_date: @state.exp_date.value
          cvv: @state.cvv
          name: @state.name
          zipcode: @state.zipcode
      else
        data =
          card_number: @state.card_number.value
          exp_date: @state.exp_date.value
          cvv: @state.cvv

      Meteor.call 'submitCreditCard', @props.app.session.id, data, (err, res) =>
        if not err
          if res.code is "F"
            # console.log "Error:", res.message
            @showPaymentNotification 'error', "Verification not successful. Please double check your credit card details."
            analytics.track 'User received an error while verifying the credit card'
          else if res.code is "S"
            @showPaymentNotification 'success', "Verification Successful!"

  showPaymentNotification: (type, message) =>
    if type is 'error'
      @setState
        payment_notification: message
      , =>
        $('#ColdCard #credit_card #next').removeClass 'loading'
        $('#ColdCard #credit_card #layer').removeClass 'visible'
        $('#ColdCard #credit_card #payment_notification').addClass 'error'
        $('#ColdCard #credit_card #payment_notification').removeClass 'hidden'
        setTimeout () =>
          $('#ColdCard #credit_card #payment_notification').removeClass 'hidden'
          setTimeout () =>
            @setState
              payment_notification: ""
          , 400
        , 8000
    else
      @setState
        payment_notification: message
        card_submitted: yes
      , =>
        $('#ColdCard #credit_card #next').removeClass 'loading'
        $('#ColdCard #credit_card #title').addClass 'hidden'
        $('#ColdCard #credit_card #card_number').addClass 'hidden'
        $('#ColdCard #credit_card #exp_date').addClass 'hidden'
        $('#ColdCard #credit_card #cvv').addClass 'hidden'
        if @props.app.PAYMENT_SERVICE is 'WePay'
          $('#ColdCard #credit_card #name').addClass 'hidden'
          $('#ColdCard #credit_card #zipcode').addClass 'hidden'
        $('#ColdCard #credit_card #payment_service').addClass 'hidden'
        $('#ColdCard #credit_card #payment_notification').removeClass 'error'
        $('#ColdCard #credit_card #payment_notification').removeClass 'hidden'

  goToQuestion: =>
    @props.changeCardsPosition 5
    document.removeEventListener 'keypress', @enterCard

  showHelp: (text) =>
    @setState
      helpVisible: yes
      helpText: text
    , ->
      setTimeout () =>
        $('#ColdCard #help').removeClass 'hidden'
      , 50

  hideHelp: =>
    $('#ColdCard #help').addClass 'hidden'
    setTimeout () =>
      @setState
        helpVisible: no
        helpText: ''
    , 100

  typeQuestion: (e) =>
    if @state.question_typing_count is 0
      analytics.track 'User started typing a question'

    @setState
      question_typing_count: @state.question_typing_count + 1

    @setState
      question: e.target.value

  enterQuestion: (e) =>
    if e.key is 'Enter'
      @sendQuestion()

  sendQuestion: =>
    Meteor.call 'sendQuestion', @props.app.session.id, @state.question, (err, res) =>
      if not err
        @props.changeCardsPosition 7



  render: =>
    <div id='ColdCard' className={ if @props.app.cards.active_card is 'cold' then 'cold_card active_card' else 'cold_card' }>
      <div id='layout'>
        {
          if @props.app.cards.cold.content is 'copyright_top'
            <p id={ if @props.app.cards.cold.content is 'copyright_top' then 'copyright_top' }>{ "© Ten Hundred #{ new Date().getFullYear() }. All Rights Reserved." }</p>
          else
            <p id={ if @props.app.cards.cold.content is 'copyright_top' then 'copyright_top' }>
              <span>{ "© Ten Hundred #{ new Date().getFullYear() }. All Rights Reserved." }</span>
              {
                if @props.app.cards.state is 1
                  <span id='terms' onClick={ @props.changeCardsPosition.bind this, 16 }>Terms of Service</span>
              }
            </p>
        }
        {
          if @state.content is 'credit_card'
            document.addEventListener 'keypress', @enterCard
            <div id='credit_card' className='hidden'>
              <div id='layout'>
                <h3 id='title'>Your credit card data</h3>
                {
                  if not @state.card_submitted
                    <div id='derigo' onMouseOver={ @showHelp.bind this, 'card' } onMouseOut={ @hideHelp }></div>
                }
                <div id='card_number'>
                  <p className='label'>Card number</p>
                  <div id='input'>
                    <input type='text' value={ @state.card_number.first } onChange={ @typeCardNumber.bind this, 1 } onFocus={ @focusCardNumber.bind this, 1 }/>
                    <input type='text' value={ @state.card_number.second } onChange={ @typeCardNumber.bind this, 2 } onFocus={ @focusCardNumber.bind this, 2 }/>
                    <input type='text' value={ @state.card_number.third } onChange={ @typeCardNumber.bind this, 3 } onFocus={ @focusCardNumber.bind this, 3 }/>
                    <input type='text' value={ @state.card_number.fourth } onChange={ @typeCardNumber.bind this, 4 } onFocus={ @focusCardNumber.bind this, 4 }/>
                  </div>
                </div>
                <div id='exp_date'>
                  <p className='label'>Exp. date</p>
                  <div id='input'>
                    <input type='text' placeholder='MM' value={ @state.exp_date.first } onChange={ @typeExpDate.bind this, 1 } onFocus={ @focusExpDate.bind this, 1 }/>
                    <input type='text' placeholder='YY' value={ @state.exp_date.second } onChange={ @typeExpDate.bind this, 2 } onFocus={ @focusExpDate.bind this, 2 }/>
                  </div>
                </div>
                <div id='cvv'>
                  <p className='label'>Cvv</p>
                  <div id='input'>
                    <input type='text' value={ @state.cvv } onChange={ @typeCvv } onFocus={ @focusCvv }/>
                  </div>
                </div>
                {
                  if @props.app.PAYMENT_SERVICE is 'WePay'
                    <div id='name'>
                      <p className='label'>Cardholder Name</p>
                      <div id='input'>
                        <input type='text' placeholder='FULL NAME' value={ @state.name } onChange={ @typeName }/>
                      </div>
                    </div>
                }
                {
                  if @props.app.PAYMENT_SERVICE is 'WePay'
                    <div id='zipcode'>
                      <p className='label'>Postal Code</p>
                      <div id='input'>
                        <input type='text' value={ @state.zipcode } onChange={ @typeZipcode }/>
                      </div>
                    </div>
                }
                <div id='payment_service'>
                  {
                    if @props.app.PAYMENT_SERVICE is 'Braintree'
                      <img src='./img/pictures/braintree_logo.png' alt='PayPal Braintree'/>
                    else
                      <img src='./img/pictures/wepay_logo.png' alt='WePay'/>
                  }
                </div>
                {
                  if @props.app.PAYMENT_SERVICE is 'WePay'
                    <div id='next' className={ if @state.card_number.value.length is 16 and @state.exp_date.value.length >= 4 and @state.cvv.length is 3 and @state.name.length >= 4 and @state.zipcode.length >= 5 and @state.content is 'credit_card' or @state.card_submitted then 'big_button warm' else 'big_button warm hidden' } onClick={ if @state.card_number.value.length is 16 and @state.exp_date.value.length >= 4 and @state.cvv.length is 3 and @state.name.length >= 4 and @state.zipcode.length >= 5 and @state.content is 'credit_card' then @submitCreditCard else if @state.card_submitted then @goToQuestion }>
                      <div id='icon'></div>
                      <p>NEXT</p>
                    </div>
                  else
                    <div id='next' className={ if @state.card_number.value.length is 16 and @state.exp_date.value.length >= 4 and @state.cvv.length is 3 and @state.content is 'credit_card' or @state.card_submitted then 'big_button warm' else 'big_button warm hidden' } onClick={ if @state.card_submitted then @goToQuestion else if @state.card_number.value.length is 16 and @state.exp_date.value.length >= 4 and @state.cvv.length is 3 and @state.content is 'credit_card' then @submitCreditCard }>
                      <div id='icon'></div>
                      <p>NEXT</p>
                    </div>
                }
                {
                  if @state.helpVisible and @state.helpText is 'card'
                    <div id='help' className='hidden'>
                      <h4>Your credit card won’t be charged until your letter is answered</h4>
                      <p>This step is for verification only. Please note that you will see an authorization on your card but you won't be charged until your question is answered.</p>
                    </div>
                }
                <div id='layer'></div>
                <p id='payment_notification' className='hidden'>{ @state.payment_notification }</p>
              </div>
            </div>
          else if @state.content is 'question'
            <div id='question' className='hidden'>
              <div id='layout'>
                <div id='title'>
                  <h3>Type your question:</h3>
                </div>
                <div id='input'>
                  <textarea rows='10' name='question' onChange={ @typeQuestion } value={ @state.question }/>
                </div>
                <div id='derigo' onMouseOver={ @showHelp.bind this, 'question' } onMouseOut={ @hideHelp }></div>
                <div id='next' className='big_button warm' onClick={ @props.changeCardsPosition.bind this, 6 }>
                  <div id='icon'></div>
                  <p>NEXT</p>
                </div>
              </div>
              {
                if @state.helpVisible and @state.helpText is 'question'
                  <div id='help' className='hidden'>
                    <h4>Suggested Format for Submissions</h4>
                    <p>How you share your question and concerns is entirely up to you, but it's easier for us to REALLY meet your needs if you provide the essential details.</p>
                    <br/>
                    <p>1. What's your concern?</p>
                    <p>2. Give us some details.</p>
                    <p>3. How can we help you?</p>
                  </div>
              }
            </div>
          else if @state.content is 'review'
            document.addEventListener 'keypress', @enterQuestion
            <div id='review' className='hidden'>
              <div id='layout'>
                <div id='title'>
                  <h3>Please review your question:</h3>
                </div>
                <div id='text'>
                  <p>{ @state.question }</p>
                </div>
                <div id='back' className='big_button' onClick={ @props.changeCardsPosition.bind this, 5 }>
                  <div id='icon'></div>
                  <p>BACK</p>
                </div>
                <div id='next' className='big_button warm' onClick={ @sendQuestion }>
                  <div id='icon'></div>
                  <p>NEXT</p>
                </div>
              </div>
            </div>
          else if @state.content is 'complete'
            <div id='complete' className='hidden'>
              <div id='layout'>
                <div id='title'>
                  <h3>Thank you!</h3>
                </div>
                <div id='text'>
                  <h3>Your question has been received.<br/>We will send a letter to your email within the next 48 hours with your ready advice.</h3>
                </div>
              </div>
            </div>
        }
      </div>
    </div>



export default outfit ColdCard
