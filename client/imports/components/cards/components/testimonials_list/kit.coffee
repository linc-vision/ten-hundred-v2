import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'



Testimonials_list = class extends Component
  constructor: (props) ->
    super props
    @state = {}



  back: =>
    @props.changeCardsPosition 1



  render: =>
    <div id='Testimonials_list' className={ if @props.app.cards.active_card is 'testimonials' then 'active_card' else 'hidden' }>
      <div id='layout'>
        <div id='testimonials_list'>
          <div id='list'>
            {
              @props.testimonials.map (item, number) =>
                <div key={ item._id } id={ item._id } className='item warm_card'>
                  <div id='layout'>
                    <div id='testimonial'>
                      {
                        if item.author.length > 0
                          <p id='label'>{ item.author }</p>
                      }
                      <p id='text'>{ item.testimonial }</p>
                    </div>
                  </div>
                </div>
            }
          </div>
        </div>
        <div id='back' className='big_button' onClick={ @back }>
          <div id='icon'></div>
          <p>BACK</p>
        </div>
      </div>
    </div>



export default outfit Testimonials_list
