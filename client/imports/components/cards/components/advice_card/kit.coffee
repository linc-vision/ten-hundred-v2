import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'


AdviceCard = class extends Component
  constructor: (props) ->
    super props
    @state =
      content: 'getAdvice'



  componentDidMount: =>
    setTimeout () =>
      $('#AdviceCard').removeClass 'hidden'
    , 400



  getAdvice: =>
    $('#AdviceCard #intro').addClass 'hidden'
    setTimeout () =>
      @setState
        content: 'showAdvice'
      , =>
        $('#AdviceCard #content').removeClass 'hidden'
    , 400





  render: =>
    <div id='AdviceCard' className='cold_card active_card hidden'>
      <div id='layout'>
        <div id='bg'></div>
        {
          if @state.content is 'getAdvice'
            <div id='intro'>
              <div id='title'>
                <h3>Get Your Advice!</h3>
                <div id='line'></div>
              </div>
              <button className='small_button' onClick={ @getAdvice }>GET</button>
            </div>
          else
            <div id='content' className='hidden'>
              <div id='title'>
                <h3>{ @props.app.advice.title }</h3>
                <div id='line'></div>
                <p>{ @props.app.advice.subtitle }</p>
              </div>
              <div id='text'>
                <p>{ @props.app.advice.text }</p>
              </div>
            </div>
        }
      </div>
    </div>



export default outfit AdviceCard
