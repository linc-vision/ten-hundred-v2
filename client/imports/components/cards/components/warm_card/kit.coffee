import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'



WarmCard = class extends Component
  constructor: (props) ->
    super props
    @state =
      content: @props.app.cards.warm.content
      email_placeholder: [ "T", "Ty", "Typ", "Type", "Type ", "Type y", "Type yo", "Type you", "Type your", "Type your ", "Type your e", "Type your em", "Type your ema", "Type your emai", "Type your email", "Type your email ", "Type your email h", "Type your email he", "Type your email her", "Type your email here", "Type your email here.", "Type your email here..", "Type your email here..." ]
      email_value: ""
      email_typing_count: 0
      verification_code:
        first: ""
        second: ""
        third: ""
        value: ""
      resendCode:
        resended: no
        value: 0
      completeState: {}



  componentWillReceiveProps: (newProps) =>
    if newProps.app.cards.state isnt @props.app.cards.state
      $('#WarmCard').transition
        transform: newProps.app.cards.warm.position
        width: newProps.app.cards.warm.width
        height: newProps.app.cards.warm.height
      , 600, 'ease-in-out', ->
        $('#WarmCard').css
          zIndex: newProps.app.cards.warm.zIndex

    setTimeout () =>
      @setState
        email_value: ""
        verification_code:
          first: ""
          second: ""
          third: ""
          value: ""
        completeState: {}
    , 800

    if newProps.app.cards.warm.content is 'type_email'
      setTimeout () =>
        @setState
          content: newProps.app.cards.warm.content
        , =>
          setTimeout () =>
            $('#WarmCard #type_email').removeClass 'hidden'
          , 800
          setTimeout () =>
            @state.email_placeholder.map (text, number) =>
              setTimeout () =>
                $('#WarmCard #type_email input').attr 'placeholder', text
              , 1600 + number * 120
          , 2000
      , 600

    else if newProps.app.cards.warm.content is 'check_verification_code'
      $('#WarmCard #type_email').addClass 'hidden'
      setTimeout () =>
        @setState
          content: newProps.app.cards.warm.content
        , ->
          $('#WarmCard #check_verification_code').removeClass 'hidden'
      , 400

    else if newProps.app.cards.warm.content is 'letter_only'
      $('#WarmCard #check_verification_code').addClass 'hidden'
      setTimeout () =>
        @setState
          content: newProps.app.cards.warm.content
        , ->
          $('#WarmCard #letter_only').removeClass 'hidden'
      , 400



  typeEmail: (e) =>
    if @state.email_typing_count is 0
      analytics.track 'User started typing email'
    @setState
      email_value: e.target.value
    , =>
      @setState
        email_typing_count: @state.email_typing_count + 1

  enterEmail: (e) =>
    if e.key is 'Enter'
      if @state.email_value.length > 5 and @state.content
        @submitEmail()

  submitEmail: =>
    Meteor.call 'submitEmail', @props.app.session.id, @state.email_value
    @props.changeCardsPosition 3
    setTimeout () =>
      @setState
        email_value: ""
    , 1000

  focusVerificationCode: (number, e) =>
    if number is 1
      if @state.verification_code.first.length > 0
        @setState
          verification_code: {
            @state.verification_code...
            first: ""
            value: "#{ @state.verification_code.second }#{ @state.verification_code.third }"
          }
    else if number is 2
      if @state.verification_code.second.length > 0
        @setState
          verification_code: {
            @state.verification_code...
            second: ""
            value: "#{ @state.verification_code.first }#{ @state.verification_code.third }"
          }
    else if number is 3
      if @state.verification_code.third.length > 0
        @setState
          verification_code: {
            @state.verification_code...
            third: ""
            value: "#{ @state.verification_code.first }#{ @state.verification_code.second }"
          }

  typeVerificationCode: (digit, e) =>
    if digit is 1
      @setState
        verification_code: {
          @state.verification_code...
          first: e.target.value
        }
      , =>
        @setState
          verification_code: {
            @state.verification_code...
            value: "#{ @state.verification_code.first }#{ @state.verification_code.second }#{ @state.verification_code.third }"
          }
        , =>
          if @state.verification_code.second.length is 0
            $('#WarmCard #check_verification_code #input input').eq(1).focus()
          else
            $('#WarmCard #check_verification_code #input input').eq(0).blur()
    else if digit is 2
      @setState
        verification_code: {
          @state.verification_code...
          second: e.target.value
        }
      , =>
        @setState
          verification_code: {
            @state.verification_code...
            value: "#{ @state.verification_code.first }#{ @state.verification_code.second }#{ @state.verification_code.third }"
          }
        , =>
          if @state.verification_code.third.length is 0
            $('#WarmCard #check_verification_code #input input').eq(2).focus()
          else
            $('#WarmCard #check_verification_code #input input').eq(1).blur()
    else if digit is 3
      @setState
        verification_code: {
          @state.verification_code...
          third: e.target.value
        }
      , =>
        @setState
          verification_code: {
            @state.verification_code...
            value: "#{ @state.verification_code.first }#{ @state.verification_code.second }#{ @state.verification_code.third }"
          }
        , =>
          $('#WarmCard #check_verification_code #input input').eq(2).blur()
          if @state.verification_code.value.length is 3
            @checkVerificationCode()

  resendVerificationCode: =>
    Meteor.call 'resendVerificationCode', @props.app.session.id, (err, res) =>
      if not err
        @setState
          resendCode:
            resended: yes
            value: 60
        , =>
          minute = setInterval () =>
            @setState
              resendCode: {
                ...@state.resendCode
                value: @state.resendCode.value - 1
              }
            , =>
              if @state.resendCode.value is 0
                @setState
                  resendCode:
                    resended: no
                    value: 60
                clearInterval minute
          , 1000

  checkVerificationCode: =>
    if @state.completeState.code is 'S'
      @props.changeCardsPosition 4
    else
      Meteor.call 'checkVerificationCode', @props.app.session.id, @state.verification_code.value, (err, res) =>
        if not err
          $('#WarmCard #check_verification_code').addClass 'hidden'
          @showMessage res

  showMessage: (res) =>
    setTimeout () =>
      @setState
        verification_code:
          first: ""
          second: ""
          third: ""
          value: ""
        completeState: res
      , =>
        setTimeout () =>
          $('#WarmCard #check_verification_code').removeClass 'hidden'
        , 400
    , 400

  nextStep: (e) =>
    if e.key is 'Enter'
      @checkVerificationCode()



  render: =>
    <div id='WarmCard' className={ if @props.app.cards.active_card is 'warm' then 'warm_card active_card' else 'warm_card' }>
      <div id='layout'>
        <img src='./img/pictures/logo_title.png' alt='Ten Hundred logo'/>
        {
          if @state.content is 'type_email'
            <div id='type_email' className='hidden' onKeyPress={ @enterEmail }>
              <div id='layout'>
                <div id='left'>
                  <div id='title'>
                    <h3>Email address</h3>
                  </div>
                  <div id='text'>
                    <p>Great to see you here, we can’t wait to hear from you! First, enter your email.</p>
                  </div>
                  <div id='input'>
                    <input type='text' name='email' placeholder='' value={ @state.email_value } onChange={ @typeEmail }/>
                  </div>
                </div>
                <div id='right'>
                  <img src='./img/pictures/letter_dark.png' alt='Ten Hundred letter'/>
                </div>
                <div id='next' className={ if @state.email_value.length > 5 and @state.content is 'type_email' then 'big_button' else 'big_button hidden' } onClick={ if @state.email_value.length > 5 and @state.content is 'type_email' then @submitEmail }>
                  <div id='icon'></div>
                  <p>NEXT</p>
                </div>
              </div>
            </div>
          else if @state.content is 'check_verification_code'
            <div id='check_verification_code' className='hidden'>
              <div id='layout'>
                <div id='left'>
                  {
                    if @state.completeState.code is 'F'
                      <div id='title'>
                        <h3 className='warning'>{ @state.completeState.message }</h3>
                        <p onClick={ @props.changeCardsPosition.bind this, 2 }>or re-enter email address</p>
                      </div>
                    else if @state.completeState.code is 'S'
                      document.addEventListener 'keypress', @nextStep
                      <div id='title'>
                        <h3>{ @state.completeState.message }</h3>
                      </div>
                    else
                      <div id='title'>
                        <h3 className='opacity_6'>We just sent you the confirmation email. Please check your inbox.</h3>
                        <p onClick={ @props.changeCardsPosition.bind this, 2 }>or re-enter email address</p>
                      </div>
                  }
                  {
                    if @state.completeState.code isnt 'S'
                      <div id='text'>
                        <h3>Type the confirmation code:</h3>
                      </div>
                  }
                  {
                    if @state.completeState.code isnt 'S'
                      <div id='input'>
                        <input type='text' value={ @state.verification_code.first } onChange={ @typeVerificationCode.bind this, 1 } onFocus={ @focusVerificationCode.bind this, 1 }/>
                        <input type='text' value={ @state.verification_code.second } onChange={ @typeVerificationCode.bind this, 2 } onFocus={ @focusVerificationCode.bind this, 2 }/>
                        <input type='text' value={ @state.verification_code.third } onChange={ @typeVerificationCode.bind this, 3 } onFocus={ @focusVerificationCode.bind this, 3 }/>
                        <p id='resendVerificationCode' className={ if @state.resendCode.resended then 'inactive' } onClick={ if not @state.resendCode.resended then @resendVerificationCode }>{ if not @state.resendCode.resended then "Resend code" else "Resended. Try again in #{ @state.resendCode.value }" }</p>
                      </div>
                  }
                </div>
                <div id='right'>
                  <img src='./img/pictures/letter_dark.png' alt='Ten Hundred letter'/>
                </div>
                <div id='next' className={ if @state.verification_code.value.length is 3 and @state.content is 'check_verification_code' or @state.completeState.code is 'S' then 'big_button' else 'big_button hidden' } onClick={ if @state.verification_code.value.length is 3 and @state.content is 'check_verification_code' or @state.completeState.code is 'S' then @checkVerificationCode }>
                  <div id='icon'></div>
                  <p>NEXT</p>
                </div>
              </div>
            </div>
          else if @state.content is 'letter_only'
            <div id='letter_only' className='hidden'>
              <img src='./img/pictures/letter_dark.png' alt='Ten Hundred letter'/>
            </div>
        }
      </div>
    </div>



export default outfit WarmCard
