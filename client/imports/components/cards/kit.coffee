import React, { Component } from 'react'
import { outfit } from '/client/imports/tools/outfit'
import $ from 'jquery.transit'

import ColdCard from './components/cold_card/kit'
import WarmCard from './components/warm_card/kit'
import BlueCard from './components/blue_card/kit'
import AdviceCard from './components/advice_card/kit'
import Testimonials_list from './components/testimonials_list/kit'
import Terms from './components/terms/kit'


Cards = class extends Component
  constructor: (props) ->
    super props
    @state = {}



  componentDidMount: =>
    setTimeout () =>
      $('#App > #layout > .cards_layout').removeClass 'hidden'
      setTimeout () =>
        $('#App > #layout > .cards_layout').css
          transition: 'none'
      , 400
    , 100

    if @props.location.search isnt ''
      Meteor.call 'getAdvice', @props.location.search, (err, res) =>
        if err
          @props.history.push ''
          @props.createSession()
        else
          if res.code is 'S'
            @props.changeCardsPosition 20
            @props.showAdviceCard res.advice
          else
            @props.history.push ''
            @props.createSession()
    else
      @props.createSession()



  render: =>
    <div className='cards_layout hidden'>
      <div id='shape'>
        <ColdCard />
        <WarmCard />
        <BlueCard />
        {
          if @props.testimonials.length > 0
            <Testimonials_list />
        }
        <Terms />
        {
          if @props.app.adviceCard
            <AdviceCard />
        }
      </div>
    </div>



export default outfit Cards
